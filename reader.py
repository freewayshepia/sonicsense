import math, pyglet, sys
from serial import Serial

########################## Color definitions ###################################
white = (0xff, 0xff, 0xff)
yellow= (0xff, 0xff, 0x00)
green = (0x00, 0xff, 0x00)
############################# Constantes #######################################
NB_SONARS = 11
ind   =(  6,   8,   3,   2,   4,   9,   1,  10,   5,   7,   0)# ordre des sonars
d_middle=(16,  15,  16,14.5,  14,  14,  16,  16,  14,  14,  14)#dist from middle
angles = (270, 130,  34,  23,  78, 211, 298, 244, 326, 103, 159)

############################# Debut du main ####################################
if len(sys.argv) < 3 :
    print("Usage   : python3 reader.py serial_path baudrate")
    print("Exemple : python3 reader.py /dev/tty.usbmodem1411 115200")
    quit()

try : 
    ser = Serial(sys.argv[1], int(sys.argv[2]))
except :
    print("port not found")
    quit()

sonar_values = NB_SONARS * [180]
point_vecteur =[0,0]
window = pyglet.window.Window()

################################################################################
##  get polar coordinate from norme * angle
#
def get_pts(angle, dist):
    x = int(dist * math.cos(math.radians(angle)) + (window.width  / 2))
    y = int(dist * math.sin(math.radians(angle)) + (window.height / 2))
    return (x,y)

################################################################################
##  draw a white square in the middle of the window
#
def draw_robot():
    largeur = int(24 // 2)              # largeur : 24 cm ( 1px = 1cm...)
    s_x, s_y = window.get_size()
    s_x = (s_x // 2)
    s_y = (s_y // 2)
    pts  = (s_x - largeur, s_y - largeur)
    pts += (s_x + largeur, s_y - largeur)
    pts += (s_x + largeur, s_y + largeur)
    pts += (s_x - largeur, s_y + largeur)

    pyglet.graphics.draw(4, pyglet.gl.GL_QUADS, ('v2i',pts), ('c3B', 4 * white))

################################################################################
##  update globals variables from serial data
#
def update(dt):
    read = lambda sig : int.from_bytes(ser.read(), byteorder='big', signed=sig)

    point_vecteur.clear()
    sonar_values.clear()
    # byte de syncronisation au debut de chaque trame
    while read(False) != 0xff : pass

    for item in range(NB_SONARS):
        sonar_values.append(read(False))

    for i in range(2):
        point_vecteur.append(read(True))
    
################################################################################
##  use point_vecteur and sonar_values to draw them on the window
#
@window.event
def on_draw():
    window.clear()
    draw_robot()
    label = lambda text,y_pos : pyglet.text.Label(text, font_name='Garamond',
                                font_size=20, x=0, y=y_pos,
                                anchor_x='left', anchor_y='top').draw()

    vecteur = ( int(window.width / 2), int(window.height / 2), 
               int(point_vecteur[0] + (window.width  / 2)), 
               int(point_vecteur[1] + (window.height / 2)))
    coll = ()
    for i in range(NB_SONARS):
        coll += get_pts(angles[ind[i]], sonar_values[ind[i]] + d_middle[ind[i]])
    
    pyglet.graphics.draw(NB_SONARS, pyglet.gl.GL_LINE_LOOP, 
                        ('v2f',coll), ('c3B', NB_SONARS * yellow))
    pyglet.graphics.draw(2,pyglet.gl.GL_LINES,('v2i',vecteur),('c3B', 2* green))

    label('coord polaire x:' + str(point_vecteur[0]),window.height- 10)
    label('coord polaire y:' + str(point_vecteur[1]),window.height- 30)
    pyglet.clock.tick()

############################## Suite du main ###################################
pyglet.clock.schedule_interval(update, 1.0/128.0)
pyglet.app.run()