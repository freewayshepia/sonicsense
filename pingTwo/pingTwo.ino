#include <SonicSense.h>
#include <Wire.h>

#define MAX_DISTANCE 200 // Maximum distance (in cm) to ping.
#define SONAR_PARAL  2
#define PING_INTERVAL 31 // ms between sensor pings (29ms is about the min to avoid cross-sensor echo).

#define COMMON_TRIG  13
#define ECHO_SONAR1  12
#define ECHO_SONAR2  11
uint8_t echo_pins[] = {ECHO_SONAR1, ECHO_SONAR2};

SonicSense sonars = SonicSense(COMMON_TRIG, echo_pins, MAX_DISTANCE);
unsigned long pingTimer;

uint8_t read_values[SONAR_PARAL];

void setup() {
    Serial.begin(115200);
    pingTimer = millis();
}

void loop() {
    if (millis() >= pingTimer) {
        pingTimer = millis() + PING_INTERVAL;
        
        sonars.read(read_values);
        
        Serial.print("Sonar 1 : "); Serial.println(read_values[0]);
        Serial.print("Sonar 2 : "); Serial.println(read_values[1]);
        
        sonars.ping(); // Do the ping (non blocking).
    }
    // The rest of your code would go here.
}