
#include <SonicSense.h>
SonicSense * SonicSense::current;


int absolute(int val){
    if (val < 0)
        return val *(-1);
    return val;
}

void Circ_Sense::next(){
    cursor = (cursor +1 == BUFFER_SIZE) ? 0 : cursor + 1;
}
    
/* the sum of the buffer
 *
 */
int Circ_Sense::sum() const{
    int somme = 0;
    for (int i = 0; i < BUFFER_SIZE; i++)
        somme += values[i];
    return somme;
}
    
/* la moyenne ...
 */
int Circ_Sense::average() const{
    return sum() / nb_val;
}
    
    
/* Constructor */
Circ_Sense::Circ_Sense()
    : cursor(0), values{0}, nb_val(0)
{}
    
/** add the value into the circle buffer
 *
 */
void Circ_Sense::add(uint8_t value){
    next();
    if (nb_val < BUFFER_SIZE)
        nb_val++;
    values[cursor] = value;
}

/* return true if it thinks the last value is valid
 *
 */
bool Circ_Sense::valid()  const{
    if (!values[cursor]) {
        return false;
    }
    else if(absolute(average() - values[cursor]) < VALID_DISTANCE){
        
        return true;
    }
    else{
        return false;
    }
}
    
uint8_t Circ_Sense::read(){
    return values[cursor];
}


// ---------------------------------------------------------------------------
// SonicSense constructor
// ---------------------------------------------------------------------------

SonicSense::SonicSense(uint8_t trigger_pin, uint8_t echo_pins[SONAR_PARAL], int max_cm_distance) {
    pinMode(trigger_pin, OUTPUT);                   // set the pin as output
    _triggerBit = digitalPinToBitMask(trigger_pin); // Get the port register bitmask for the trigger pin.
    _triggerOutput = portOutputRegister(digitalPinToPort(trigger_pin)); // Get the output port register for the trigger pin.
    
    for (int i = 0; i < SONAR_PARAL; i++){// Get the input port register for the echo pin.
        _echoInput[i] = portInputRegister(digitalPinToPort(echo_pins[i]));
        _echoBit[i] = digitalPinToBitMask(echo_pins[i]);       // Get the port register bitmask for the echo pin.
    }
    
    _triggerMode = (uint8_t *) portModeRegister(digitalPinToPort(trigger_pin)); // Get the port mode register for the trigger pin.
    
    _maxEchoTime = min(max_cm_distance, MAX_SENSOR_DISTANCE) * US_ROUNDTRIP_CM + (US_ROUNDTRIP_CM / 2); // Calculate the maximum distance in uS.
    
}


// ---------------------------------------------------------------------------
// Standard ping method support functions (not called directly)
// ---------------------------------------------------------------------------

boolean SonicSense::ping_trigger() {
    *_triggerOutput &= ~_triggerBit; // Set the trigger pin low, should already be low, but this will make sure it is.
    delayMicroseconds(4);            // Wait for pin to go low, testing shows it needs 4uS to work every time.
    *_triggerOutput |= _triggerBit;  // Set trigger pin high, this tells the sensor to send out a ping.
    delayMicroseconds(10);           // Wait long enough for the sensor to realize the trigger pin is high. Sensor specs say to wait 10uS.
    *_triggerOutput &= ~_triggerBit; // Set trigger pin back to low.

    
    _max_time =  micros() + MAX_SENSOR_DELAY; // Set a timeout for the ping to trigger.
    while (*_echoInput[0] & _echoBit[0] && micros() <= _max_time) {} // Wait for echo pin to clear.
    while (!(*_echoInput[0] & _echoBit[0]))                          // Wait for ping to start.
        if (micros() > _max_time) return false;                // Something went wrong, abort.
    _max_time = micros() + _maxEchoTime; // Ping started, set the timeout.
    return true;                         // Ping started successfully.
}


// ---------------------------------------------------------------------------
// Timer2/Timer4 interrupt methods (can be used for non-ultrasonic needs)
// ---------------------------------------------------------------------------

// Variables used for timer functions
void (*intFunc)();
void (*intFunc2)();
unsigned long _ms_cnt_reset;
volatile unsigned long _ms_cnt;


// ---------------------------------------------------------------------------
// Timer interrupt ping methods (won't work with ATmega8 and ATmega128)
// ---------------------------------------------------------------------------

void SonicSense::ping() {
    timer_stop();                    // Double check.
    _timer_pin_bitmap = 0;
    current = this;
    if (!ping_trigger()) return;         // Trigger a ping, if it returns false, return without starting the echo timer.
    timer_us(ECHO_TIMER_FREQ); // Set ping echo timer check every ECHO_TIMER_FREQ uS.
}


// ---------------------------------------------------------------------------
// Utilities
// ---------------------------------------------------------------------------

void SonicSense::read(uint8_t read_values[SONAR_PARAL]){
    for (int i = 0; i < SONAR_PARAL; i++) {
        read_values[i] = values[i].read();
    }
}

void SonicSense::read_average(uint8_t read_values[SONAR_PARAL]){
    for (int i = 0; i < SONAR_PARAL; i++) {
        read_values[i] = values[i].average();
    }
}

bool SonicSense::is_valid(uint8_t sonar_num){
    if (sonar_num < SONAR_PARAL) {
        return values[sonar_num].valid();
    }
    return false;
}

uint8_t SonicSense::check_timer(){
    uint32_t val;
    
    if (micros() > _max_time) { // Outside the timeout limit.
        timer_stop();           // Disable timer interrupt
        for (int i = 0; i < SONAR_PARAL; i++) {
            if ((~_timer_pin_bitmap & (1 << i))) {
                values[i].add(NO_ECHO);
            }
        }
        return -1;           // Cancel ping timer.
    }
    
    for (int i = 0; i < SONAR_PARAL; i++) {
        if((!(*_echoInput[i] & _echoBit[i])) && (~_timer_pin_bitmap & (1 << i))){
            _timer_pin_bitmap |= (1 << i);
            values[i].add((micros() - (_max_time - _maxEchoTime) - 13)/ US_ROUNDTRIP_CM); // Calculate ping time, 13uS of overhead.
            return i;
        }
    }
    return -1;
}
// ---------------------------------------------------------------------------
// Timer2/Timer4 interrupt method support functions (not called directly)
// ---------------------------------------------------------------------------

void SonicSense::timer_setup() {
#if defined (__AVR_ATmega32U4__) // Use Timer4 for ATmega32U4 (Teensy/Leonardo).
    timer_stop(); // Disable Timer4 interrupt.
    TCCR4A = TCCR4C = TCCR4D = TCCR4E = 0;
    TCCR4B = (1<<CS42) | (1<<CS41) | (1<<CS40) | (1<<PSR4); // Set Timer4 prescaler to 64 (4uS/count, 4uS-1020uS range).
    TIFR4 = (1<<TOV4);
    TCNT4 = 0;    // Reset Timer4 counter.
#else
    timer_stop();           // Disable Timer2 interrupt.
    ASSR &= ~(1<<AS2);      // Set clock, not pin.
    TCCR2A = (1<<WGM21);    // Set Timer2 to CTC mode.
    TCCR2B = (1<<CS22);     // Set Timer2 prescaler to 64 (4uS/count, 4uS-1020uS range).
    TCNT2 = 0;              // Reset Timer2 counter.
#endif
}

#if defined (__AVR_ATmega32U4__) // Use Timer4 for ATmega32U4 (Teensy/Leonardo).
ISR(TIMER4_OVF_vect)
#else
ISR(TIMER2_COMPA_vect)
#endif
{
    // Problem occurs from 3 parallals sensors, interrupt routine is too long
    SonicSense::current->check_timer();
}


void SonicSense::timer_us(unsigned int frequency) {
    timer_setup();      // Configure the timer interrupt.
    
#if defined (__AVR_ATmega32U4__) // Use Timer4 for ATmega32U4 (Teensy/Leonardo).
    OCR4C = min((frequency>>2) - 1, 255); // Every count is 4uS, so divide by 4 (bitwise shift right 2) subtract one, then make sure we don't go over 255 limit.
    TIMSK4 = (1<<TOIE4);                  // Enable Timer4 interrupt.
#else
    OCR2A = min((frequency>>2) - 1, 255); // Every count is 4uS, so divide by 4 (bitwise shift right 2) subtract one, then make sure we don't go over 255 limit.
    TIMSK2 |= (1<<OCIE2A);                // Enable Timer2 interrupt.
#endif
}

void SonicSense::timer_stop() { // Disable timer interrupt.
#if defined (__AVR_ATmega32U4__) // Use Timer4 for ATmega32U4 (Teensy/Leonardo).
    TIMSK4 = 0;
#else
    TIMSK2 &= ~(1<<OCIE2A);
#endif
}
