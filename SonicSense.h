#ifndef SonicSense_h
#define SonicSense_h

#if defined(ARDUINO) && ARDUINO >= 100
#include <Arduino.h>
#else
#include <WProgram.h>
#include <pins_arduino.h>
#endif

#include <avr/io.h>
#include <avr/interrupt.h>

#define SONAR_PARAL  4
// Shoudln't need to changed these values unless you have a specific need to do so.
#define MAX_SENSOR_DISTANCE 500 // Maximum sensor distance can be as high as 500cm, no reason to wait for ping longer than sound takes to travel this distance and back.
#define US_ROUNDTRIP_CM 57      // Microseconds (uS) it takes sound to travel round-trip 1cm (2cm total), uses integer to save compiled code space.

#define NO_ECHO 200               // Value returned if there's no ping echo within the specified MAX_SENSOR_DISTANCE or max_cm_distance.
#define MAX_SENSOR_DELAY 18000  // Maximum uS it takes for sensor to start the ping (SRF06 is the highest measured, just under 18ms).
#define ECHO_TIMER_FREQ 24      // Frequency to check for a ping echo (every 24uS is about 0.4cm accuracy).
#define PING_MEDIAN_DELAY 29    // Millisecond delay between pings in the ping_median method.

// Conversion from uS to distance (round result to nearest cm or inch).
#define NewPingConvert(echoTime, conversionFactor) (max((echoTime + conversionFactor / 2) / conversionFactor, (echoTime ? 1 : 0)))

#define VALID_DISTANCE 15
#define BUFFER_SIZE 4

class Circ_Sense{
private:
    int cursor;                    // postition of the head
    uint8_t values[BUFFER_SIZE];   // the buffer
    int nb_val;                    // number of value into the buffer
    
    void next();
    int sum() const;
    
    
public:
    Circ_Sense();
    int average() const;
    void add(uint8_t value);
    bool valid()  const;
    uint8_t read();
};


class SonicSense {
public:
    SonicSense(uint8_t trigger_pin, uint8_t echo_pins[SONAR_PARAL], int max_cm_distance = MAX_SENSOR_DISTANCE);
    unsigned int ping_cm();
    unsigned int convert_cm(unsigned int echoTime);
    void ping();
    unsigned long ping_result;
    static void timer_us(unsigned int frequency);
    static void timer_ms(unsigned long frequency, void (*userFunc)(void));
    static void timer_stop();
    void read(uint8_t read_values[SONAR_PARAL]);
    void read_average(uint8_t read_values[SONAR_PARAL]);
    bool is_valid(uint8_t sonar_num);
    static SonicSense * current;
    uint8_t check_timer();
private:
    boolean ping_trigger();
    boolean ping_wait_timer();
    uint8_t _triggerBit;
    uint8_t _echoBit[SONAR_PARAL];
    Circ_Sense values[SONAR_PARAL];
    volatile uint8_t *_triggerOutput;
    volatile uint8_t *_triggerMode;
    volatile uint8_t *_echoInput[SONAR_PARAL];
    uint32_t _timer_pin_bitmap;
    unsigned int _maxEchoTime;
    unsigned long _max_time;
    static void timer_setup();
    static void timer_ms_cntdwn();
};

#endif