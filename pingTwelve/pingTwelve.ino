#include <SonicSense.h>
//#include <Wire.h>
#include <ArduinoJson.h>

#define I2CADDRESS   0x40

#define MAX_DISTANCE 200 // Maximum distance (in cm) to ping.
#define SONAR_PARAL  4
#define PING_INTERVAL 31 // ms between sensor pings (29ms is about the min to avoid cross-sensor echo).

#define COMMON_TRIG  13
#define COMMON_TRIG2 12
#define COMMON_TRIG3 A0

#define ECHO_SONAR1  11     // 74
#define ECHO_SONAR2  10     // 70
#define ECHO_SONAR3  9      // 67
#define ECHO_SONAR4  8      // X

#define ECHO_SONAR5  7      // 66
#define ECHO_SONAR6  6      // 68
#define ECHO_SONAR7  5      // 72
#define ECHO_SONAR8  4      // 64

#define ECHO_SONAR9  A5     // 73
#define ECHO_SONAR10 A4     // 65
#define ECHO_SONAR11 A3     // 69
#define ECHO_SONAR12 A2     // 71

uint8_t echo_pins1[] = {ECHO_SONAR1, ECHO_SONAR2, ECHO_SONAR3, ECHO_SONAR4};
uint8_t echo_pins2[] = {ECHO_SONAR5, ECHO_SONAR6, ECHO_SONAR7, ECHO_SONAR8};
uint8_t echo_pins3[] = {ECHO_SONAR9, ECHO_SONAR10, ECHO_SONAR11, ECHO_SONAR12};

SonicSense sonars[3] = {SonicSense(COMMON_TRIG, echo_pins1, MAX_DISTANCE),
    SonicSense(COMMON_TRIG2, echo_pins2, MAX_DISTANCE),
    SonicSense(COMMON_TRIG3, echo_pins3, MAX_DISTANCE)};
#define NB_WPUB 14

String inputString;
bool stringComplete = false;

unsigned long pingTimer;

uint8_t read_values[SONAR_PARAL];
uint8_t turn = 0;                             // keep track of which trigger must be launched.

/************************************ Vectorial related stuff ******************************/

bool fresh_data = false;                      // on true : calculate repulsive vectors.
//angles = { 270, 130, 54, 00, 29, 78, 211, 298, 244, 326, 103, 159};
uint8_t coeff[] = { 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1};
uint8_t offset[]= { 1, 3, 3, 0, 1, 1, 4, 10, 10, 4, 1, 1};

double x_cos[] = {0.0000000000000000, -0.6427876096865393,
    0.5877852522924732, 1.000000000000000,
    0.8746197071393957, 0.20791169081775945,
    -0.8571673007021123, 0.4694715627858904,
    -0.43837114678907774, 0.8290375725550418,
    -0.22495105434386503, -0.9335804264972017};

double y_sin[] = {-1.000000000000000, 0.7660444431189781,
    0.8090169943749473, 0.000000000000000000,
    0.48480962024633706, 0.9781476007338056,
    -0.5150380749100542, -0.8829475928589271,
    -0.8987940462991668, -0.5591929034707466,
    0.9743700647852352, 0.3583679495453002};

/*******************************************************************************************/
void setup() {
    Serial.begin(115200);
    pingTimer = millis();
}

void loop() {
    if (millis() >= pingTimer) {
        pingTimer = millis() + PING_INTERVAL;
        if(turn == 2){
            fresh_data = true;
        }
        
        sonars[turn].ping(); // Do the ping (non blocking).
        turn = (turn + 1 == 3) ? 0 : turn + 1;
    }
    if (fresh_data){
        float norme = 0.0;
        double polaire_x = 0.0, polaire_y = 0.0;
        StaticJsonBuffer<200> jsonBuffer2;
        JsonObject& json_send = jsonBuffer2.createObject();
        JsonArray& data = json_send.createNestedArray("ultrasonic");
        
        for(int i = 0; i < 3; i++){
            sonars[i].read_average(read_values);
            for(int j = 0; j < SONAR_PARAL; j++){
                if(i!=0 || j!=04)
                    data.add(read_values[j]);
                norme = (float)coeff[(i * 4) + j]*((float)127.0 / (float)read_values[j]);
                polaire_x += (-(double)norme * x_cos[(i * 4) + j]);
                polaire_y += (-(double)norme * y_sin[(i * 4) + j]);
            }
        }
        
        JsonArray& data2 = json_send.createNestedArray("avoid_direction");
        data2.add(polaire_x,4);
        data2.add(polaire_y,4);
        json_send.printTo(Serial);
        Serial.println();

        fresh_data = false;
    }
}